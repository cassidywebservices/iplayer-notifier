from django.conf.urls import patterns, include, url
from notifierapp.views import HomePageView, SettingsView, DisclaimerView, BrowseView, AddView, RemoveView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^accounts/profile$', SettingsView.as_view(), name='settings'),
    url(r'^browse$', BrowseView.as_view(), name='browse'),
    url(r'^browse/page/(?P<page>\d+)$', BrowseView.as_view(), name='browse'),
    url(r'^browse/search/(?P<search>(.*?))$', BrowseView.as_view(), name='browse'),
    url(r'^browse/search/(?P<search>(.*?))/page/(?P<page>\d+)$', BrowseView.as_view(), name='browse'),
    url(r'^browse/by-letter/(?P<letter>[@A-Z])$', BrowseView.as_view(), name='browse'),
    url(r'^browse/by-letter/(?P<letter>[@A-Z])/page/(?P<page>\d+)$', BrowseView.as_view(), name='browse'),
    url(r'^disclaimer$', DisclaimerView.as_view(), name='disclaimer'),
    url(r'^add/(?P<id>[\da-z]+)$', AddView.as_view(), name='add'),
    url(r'^remove/(?P<id>[\da-z]+)$', RemoveView.as_view(), name='remove'),
    # url(r'^iplayernotifier/', include('iplayernotifier.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),


    (r'^accounts/', include('allauth.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
