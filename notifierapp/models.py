from django.db import models
from django.conf import settings
from datetime import datetime

class Settings(models.Model):
	class Meta:
		verbose_name = "User Settings"
		verbose_name_plural = "User Settings" 
	
	user = models.OneToOneField(settings.AUTH_USER_MODEL)
	xmpp = models.BooleanField(default=False)
	email = models.BooleanField(default=True)
	ignorerepeats = models.BooleanField(default=False)
	
	def _get_email(self):
		return self.user.email
	def _set_email(self, value):
		self.user.email = value
		
	email_address = property(_get_email, _set_email)
	xmpp_address = models.EmailField(null=True)
	
	def __unicode__(self):
		return self.user.username
	
# Programme Details
class Programme(models.Model):
	id = models.CharField(null=False, max_length=255, primary_key=True)
	name = models.CharField(null=False, max_length=255)
	synopsis = models.TextField()
	type = models.CharField(max_length=5, null=True)
	station = models.CharField(max_length=50, null=True)
	def __repr__(self):
		return self.name
		
	def __unicode__(self):
		return self.name

# Episode Details
class Episode(models.Model):
	id = models.CharField(null=False, max_length=255, primary_key=True)
	name = models.CharField(null=False, max_length=255)
	programme = models.ForeignKey(Programme, null=False)
	expires = models.DateTimeField(null=False, default=datetime.max)
	synopsis = models.TextField()
	firstairdate = models.DateTimeField(null=False, default=datetime.min)
	broadcast = models.DateTimeField(null=False, default=datetime.max)
	def __repr__(self):
		return self.name


# Notification Subscription
class Subscription(models.Model):
	class Meta:
		unique_together = ('user', 'programme')
	user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False)
	programme = models.ForeignKey(Programme, null=False)
	def __repr__(self):
		return '<' + str(self.user) + ':' + self.programme.name + '>'
