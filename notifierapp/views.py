from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, RedirectView
from django.views.generic.edit import UpdateView
from forms import SettingsForm, SearchForm
from models import Programme, Settings, Subscription
from notifierapp import LETTERS, processprogramme

class HomePageView(TemplateView):
    template_name = "home.djhtml"
    
    def get_context_data(self, **options):
        context = super(HomePageView, self).get_context_data(**options)

        new_context = {
            'users' : get_user_model().objects.count(),
            'shows' : Programme.objects.exclude(subscription=None).count(),
            'title' : 'Home',
        }
        
        context.update(new_context)
        
        return context
    
class SettingsView(UpdateView):
    model = Settings
    form_class = SettingsForm
    template_name_suffix = "_update_form"
    success_url = "/accounts/profile"
    
    def get_context_data(self, **options):
        context = super(SettingsView, self).get_context_data(**options)
        context['title'] = "Settings"
        return context
    
    def get_object(self):
        try:
            settings = self.request.user.settings
        except Settings.DoesNotExist:
            settings = Settings(user=self.request.user)
            settings.save()
        return settings
    
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return UpdateView.dispatch(self, request, *args, **kwargs)
    
class DisclaimerView(TemplateView):
    template_name = "disclaimer.djhtml"
    
    def get_context_data(self, **options):
        context = super(DisclaimerView, self).get_context_data(**options)
        context['title'] = "Disclaimer"
        return context
    
class BrowseView(ListView):
    template_name= "programme_list.djhtml"
    letter = None
    page = 1
    search = None
    
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return ListView.dispatch(self, request, *args, **kwargs)
    
    def get_context_data(self, *args, **kwargs):
        context = super(BrowseView, self).get_context_data(*args, **kwargs)
        new_context = {
            'letters' : LETTERS,
            'title' : 'Browse',
            'term' : self.search,
            'letter' : self.letter,
        }
        context.update(new_context)
        return context
    
    def get_queryset(self):
        queryset = None
        
        if 'letter' in self.kwargs:
            self.letter = self.kwargs['letter']
        if 'page' in self.kwargs:
            self.page = self.kwargs['page']
        if 'search' in self.kwargs:
            self.search = self.kwargs['search']
        
        if self.letter is not None:
            if self.letter == '@':
                queryset = Programme.objects.filter(name__range=(' ', 'A')).order_by('name')
            else:
                queryset = Programme.objects.filter(name__istartswith=self.letter).order_by('name')
        elif self.search is not None:
            queryset = Programme.objects.filter(name__icontains=self.search).order_by('name')
        else:
            queryset = Programme.objects.all()
            
        paginator = Paginator(queryset, 20)
        return paginator.page(self.page)
        
    def post(self, *args, **kwargs):
        form = SearchForm(self.request.POST)
        if form.is_valid():
            return redirect('browse', search=form.cleaned_data['term'])
        else:
            return self.get(*args, **kwargs)
        
class AddView(RedirectView):
    
    permanent = False
    
    def get_redirect_url(self, **kwargs):
        try:
            p = Programme.objects.get(id = kwargs['id'])
            s, created = Subscription.objects.get_or_create(user = self.request.user, programme=p)
            if created:
                s.save()
            if not p.episode_set.exists():
                processprogramme(p)
        except:
            pass
        return reverse("settings")
    
class RemoveView(RedirectView):
    
    permanent = False
    
    def get_redirect_url(self, **kwargs):
        p = Programme.objects.get(id = kwargs['id'])
        Subscription.objects.filter(user = self.request.user, programme=p).delete()
        return reverse("settings")
