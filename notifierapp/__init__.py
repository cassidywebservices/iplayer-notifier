#
# Common Functions used in more than one place
# Part of BBC iPlayer Notifier
# (c)2011 Andrew Cassidy <andee@bytz.co.uk>
#
from django.conf import settings
from django.core.mail import send_mass_mail
from django.template.loader import render_to_string
from datetime import datetime
from models import Programme, Episode, Subscription
from pyxmpp2.simple import send_message
import json, urllib2, re, iso8601

# URL Format String
EPISODES_URL_FORMAT = 'http://open.live.bbc.co.uk/aps/programmes/%s/episodes/player.json'
PROGRAMMES_URL = 'http://open.live.bbc.co.uk/aps/programmes/a-z/by/%s.json'
LETTERS = [chr(x+64) for x in range(0,27)]

# process the given show
def processprogramme(programme):
    # list to store all new episodes in, so we can do a single email for multiple
    new = []
    # the url to the json iplayer list
    url = EPISODES_URL_FORMAT % programme.id
    # open the url
    try:
        f = urllib2.urlopen(url)
    except:
        return new
    # read the json into an object we can use
    j = json.load(f)
    # close the url file handle
    f.close()
    
    # go through the list
    for episode in j["episodes"]:
        # see if we already have a record of the episode
        q = Episode.objects.filter(id=episode["programme"]["pid"])
        if not q.exists():
            # we don't so it's new
            # sort out the time stuff
            dt = datetime.max;
            if (episode["programme"].get("available_until", None) != None):
                dt = iso8601.parse_date(episode["programme"]["available_until"])
            
            bcdate = datetime.now()
            if (episode["programme"].get("first_broadcast_date", None) != None):
                bcdate = iso8601.parse_date(episode["programme"]["first_broadcast_date"])
                
            showdate = datetime.now()
            if (episode["programme"].get("actual_start", None) != None):
                showdate = iso8601.parse_date(episode["programme"]["actual_start"])
            
            p = Episode(programme = programme,
                id = episode["programme"]["pid"], 
                name = episode["programme"]["title"], 
                expires = dt,
                synopsis = episode["programme"]["short_synopsis"],
                firstairdate = bcdate,
                broadcast = showdate)
            # save to datastore and add it to our list
            p.save()
            new.append(p)
    return new

def loadprogrammes():
    for letter in LETTERS:
        try:
            # open the url
            f = urllib2.urlopen(PROGRAMMES_URL % letter)
            # read the json into an object we can use
            j = json.load(f)
            # close the url file handle
            f.close()
    
            # for each programme in the list
            for programme in j['atoz']['tleo_titles']:
                # see if we already have it in the data store
                try:
                    p = Programme.objects.get(id=programme['programme']['pid'])
                    if (not p.type or not p.station) and 'ownership' in programme['programme']:
                        p.type = programme['programme']['ownership']['service']['type']
                        p.station = programme['programme']['ownership']['service']['id']
                        p.save()
                except Programme.DoesNotExist:
                    # make sure the title is actually a string
                    progname = programme['programme']["title"]
                    # create the new programe object
                    p = Programme(id = programme['programme']['pid'], name = progname)
                    # if there is a synopsis, set it
                    if "short_synopsis" in programme['programme']:
                        p.synopsis = programme['programme']["short_synopsis"]
                    # store the new object
                    if 'ownership' in programme['programme']:
                        p.type = programme['programme']['ownership']['service']['type']
                        p.station = programme['programme']['ownership']['service']['id']
                    p.save()
    
        except urllib2.URLError:
            # if there was an errror, there are no more pages for this letter
                pass
        #except:
            # any other error, sleep then try again
            #time.sleep(2)

def process(programme):
    new = processprogramme(programme)
    
    if (new != []):
        # our email template values
        subject = '[iPlayer Notifier] New ' + programme.name + ' Available!'
        template_values = {
            'programme':programme,
            'episodes':new
        }
        
        # render the email from the template
        body = render_to_string('email.txt', template_values)
        
        f = filter(lambda x: x.firstairdate == x.broadcast, new)
        template_values['episodes'] = f
        filteredbody = render_to_string('email.txt', template_values)
        
        emails = []
        xmpps = []
        
        # get all subscribers to this programme
        q = Subscription.objects.filter(programme=programme)
        # queue the outbound emails and messages
        for sub in q:
                            
            if (sub.user.settings.ignorerepeats):
                if (f != []):
                    if (sub.user.settings.email):
                        emails.append((subject, filteredbody, 'iPlayer Notifier <noreply@iplayernotifier.co.uk>', [sub.user.email]))
                    if (sub.user.settings.xmpp and sub.user.settings.xmpp_address):
                        xmpps.append((sub.user.settings.xmpp_address, filteredbody))
            else:
                if (sub.user.settings.email):
                    emails.append((subject, body, 'iPlayer Notifier <noreply@iplayernotifier.co.uk>', [sub.user.email]))
                if (sub.user.settings.xmpp and sub.user.settings.xmpp_address):
                        xmpps.append((sub.user.settings.xmpp_address, body))
        if emails != []:
            send_mass_mail(emails)
        if xmpps != []:
            for message in xmpps:
                try:
                    send_message(settings.XMPP_JID, settings.XMPP_PASSWORD, message[0], message[1])
                except:
                    pass

def episodes():
    # get all subscriptions
    q = Subscription.objects.all()
    # a list of ones already being processed
    processed = []
    # iterate through the subs
    for sub in q:
        # check we're not already processing it
        if sub.programme not in processed:
            processed.append(sub.programme)
            # process the programme for new episodes
            process(sub.programme)
