from django.forms import ModelForm
from models import Settings
from django import forms

class SettingsForm(ModelForm):
    email_address = forms.CharField(required=True)

    class Meta:
        model = Settings
        fields = ('email','xmpp','xmpp_address','ignorerepeats')
    
    def __init__(self, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)
 
        if kwargs.has_key('instance'):
            instance = kwargs['instance']
            self.initial['email_address'] = instance.user.email
            
    def save(self, commit=True):
        model = super(SettingsForm, self).save(commit=False)
 
        model.user.email = self.cleaned_data['email_address']
 
        if commit:
            model.save()
            if model.user.email and len(model.user.email) > 1:
                model.user.save()
 
        return model
    
class SearchForm(forms.Form):
    term = forms.CharField()