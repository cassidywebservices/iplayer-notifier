from django.core.management.base import NoArgsCommand
from notifierapp import episodes

class Command(NoArgsCommand):
    def handle(self, *args, **options):
        episodes()