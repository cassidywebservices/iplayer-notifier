from django.core.management.base import NoArgsCommand
from notifierapp import loadprogrammes

class Command(NoArgsCommand):
	def handle(self, *args, **options):
		loadprogrammes()