from django.contrib import admin
from models import Settings, Programme, Episode, Subscription

class ProgrammeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'synopsis', 'type', 'station')
    readonly_fields = ('id', 'name', 'synopsis', 'type', 'station')

admin.site.register(Programme, ProgrammeAdmin)

class EpisodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'programme', 'expires')
    readonly_fields = ('id', 'name', 'programme', 'expires')
    search_fields = ['name', 'programme__name']
    
admin.site.register(Episode, EpisodeAdmin)

class SettingsAdmin(admin.ModelAdmin):
    list_display = ('user',)
    readonly_fields = ('user',)
    
admin.site.register(Settings, SettingsAdmin)
