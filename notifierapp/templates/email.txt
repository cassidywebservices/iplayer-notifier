Hi, a new episode of {{ programme.name }} is avaiable on iPlayer!

{% for episode in episodes %}
{{ episode.name }} - {{ episode.synopsis }} http://www.bbc.co.uk/iplayer/episode/{{ episode.id }}
{% endfor %}

Thanks for using iPlayer Notifier! http://www.iplayernotifier.co.uk/
